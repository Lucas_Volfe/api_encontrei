<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = [
        'nome',
        'categoria',
        'created_at',
        'updated_at',

    ];

    public function categoria()
    {
        return $this->belongsTo(Anuncio::class);
    }
}
