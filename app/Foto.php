<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $fillable = [
        'anuncio_id',
        'foto',
        'created_at',
        'updated_at',
    ];

    public function foto()
    {
        return $this->belongsTo(Anuncio::class);
    }
}
