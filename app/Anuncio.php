<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anuncio extends Model
{
    protected $fillable = [
        'titulo',
        'descricao',
        'cep',
        'bairro',
        'localidade',
        'nota_avaliacao',
        'comentario_avaliacao',
        'ativado',
        'aprovado',
        'anunciante_id',
        'categoria_id',
        'created_at',
        'updated_at',
    ];

    public function anuncios()
    {
        return $this->belongsTo(User::class);
    }

    public function getAtivadoAttribute($ativado): bool
    {
        return $ativado;
    }

    public function getAprovadoAttribute($aprovado): bool
    {
        return $aprovado;
    }

    public function getCreatedAtAttribute($date)
    {
        $toDate = date_create($date);
        return date_format(
            date_sub($toDate, date_interval_create_from_date_string('3 hours')),
            'Y-m-d H:i:s'
        );
    }

    public function getUpdatedAtAttribute($date)
    {
        $toDate = date_create($date);
        return date_format(
            date_sub($toDate, date_interval_create_from_date_string('3 hours')),
            'Y-m-d H:i:s'
        );
    }

    public function getCategoriaIdAttribute($categoria_id)
    {
        $categoria = Categoria::find($categoria_id);
        return $categoria->attributes['nome'];
    }


}
