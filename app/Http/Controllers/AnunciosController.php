<?php
namespace App\Http\Controllers;

use App\Anuncio;

class AnunciosController extends BaseController
{
    public function __construct()
    {
        $this->classe = Anuncio::class;
    }
}
