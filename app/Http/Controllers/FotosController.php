<?php
namespace App\Http\Controllers;

use App\Foto;

class FotosController extends BaseController
{
    public function __construct()
    {
        $this->classe = Foto::class;
    }
}
