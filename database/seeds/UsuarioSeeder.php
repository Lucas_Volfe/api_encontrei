<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'email' => 'teste@email',
            'password' => Hash::make('senha'),
            'username' => 'username',
            'cep' => '89803605',
            'bairro' => 'bairro',
            'localidade' => 'localidade',
            'tipo_perfil' => 1,
            'foto_perfil' => null
        ]);
    }
}
