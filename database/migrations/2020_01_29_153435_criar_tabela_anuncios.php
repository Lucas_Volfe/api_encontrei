<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTabelaAnuncios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anuncios', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('titulo', 50);
            $table->string('descricao');
            $table->string('bairro', 50);
            $table->string('localidade', 50);
            $table->string('cep', 10);
            $table->float('nota_avaliacao');
            $table->string('comentario_avaliacao', 100);
            $table->boolean('ativado')->default(false);
            $table->boolean('aprovado')->default(false);

            $table->unsignedTinyInteger('anunciante_id');
            $table->foreign('anunciante_id')
                ->references('id')
                ->on('usuarios');

            $table->unsignedTinyInteger('categoria_id');
            $table->foreign('categoria_id')
                ->references('id')
                ->on('categorias');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anuncios');
    }

}
