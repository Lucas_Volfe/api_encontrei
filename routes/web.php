<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var \Laravel\Lumen\Routing\Router $router */
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
//$router->group(['prefix' => 'api', 'middleware' => 'autenticador'], function () use ($router) {
    $router->group(['prefix' => 'series'], function () use ($router) {
        $router->post('', 'SeriesController@store');
        $router->get('', 'SeriesController@index');
        $router->get('{id}', 'SeriesController@show');
        $router->put('{id}', 'SeriesController@update');
        $router->delete('{id}', 'SeriesController@destroy');

        $router->get('{serieId}/episodios', 'EpisodiosController@buscaPorSerie');
    });

    $router->group(['prefix' => 'episodios'], function () use ($router) {
        $router->post('', 'EpisodiosController@store');
        $router->get('', 'EpisodiosController@index');
        $router->get('{id}', 'EpisodiosController@show');
        $router->put('{id}', 'EpisodiosController@update');
        $router->delete('{id}', 'EpisodiosController@destroy');
    });

    $router->group(['prefix' => 'categorias'], function () use ($router) {
        $router->post('', 'CategoriasController@store');
        $router->get('', 'CategoriasController@index');
        $router->get('{id}', 'CategoriasController@show');
        $router->put('{id}', 'CategoriasController@update');
        $router->delete('{id}', 'CategoriasController@destroy');
    });

    $router->group(['prefix' => 'anuncios'], function () use ($router) {
        $router->post('', 'AnunciosController@store');
        $router->get('', 'AnunciosController@index');
        $router->get('{id}', 'AnunciosController@show');
        $router->put('{id}', 'AnunciosController@update');
        $router->delete('{id}', 'AnunciosController@destroy');
    });

    $router->group(['prefix' => 'fotos'], function () use ($router) {
        $router->post('', 'FotosController@store');
    });

    });


$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('login', 'TokenController@gerarToken');
    $router->post('user', 'UsersController@newUser');
    $router->post('check_email_exist', 'UsersController@checkEmailExist');
    $router->post('check_username_exist', 'UsersController@checkUsernameExist');
});

